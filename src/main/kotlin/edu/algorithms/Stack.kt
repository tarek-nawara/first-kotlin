package edu.algorithms

/**
 * Implementation of stack using linked list.
 *
 * @author tarek-nawara
 */
class Stack<T> : Iterable<T> {
    private val list = SingleLinkedList<T>()

    override fun iterator(): Iterator<T> = list.iterator()

    /**
     * Adds new element on top of the stack.
     *
     * @param elem element to add
     */
    fun push(elem: T) {
        list.add(elem)
    }

    /**
     * Remove top of the stack.
     *
     * @throws IndexOutOfBoundsException if empty stack
     * @return element on top of the stack
     */
    fun pop(): T {
        if (isEmpty()) {
            throw IndexOutOfBoundsException()
        }
        return list.remove(0)!!
    }

    /**
     * Get the element on top of the stack.
     *
     * @throws IndexOutOfBoundsException if empty stack
     * @return element on top of the stack
     */
    fun peek(): T {
        if (isEmpty()) {
            throw IndexOutOfBoundsException()
        }
        return list[0]
    }

    fun isEmpty(): Boolean = list.isEmpty()
    fun nonEmpty(): Boolean = !isEmpty()
    fun size(): Int = list.size()
}