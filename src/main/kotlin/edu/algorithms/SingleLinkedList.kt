package edu.algorithms

import java.util.*

/**
 * Implementation of single linked list in kotlin
 *
 * @author tarek-nawara
 */
class SingleLinkedList<T> : Cloneable, Iterable<T> {
    companion object {
        operator fun <T> invoke(vararg data: T): SingleLinkedList<T> {
            return data.fold(SingleLinkedList()) { acc, elem -> acc.append(elem) }
        }

        operator fun <T> invoke(iterable: Iterable<T>): SingleLinkedList<T> {
            return iterable.fold(SingleLinkedList()) { acc, elem -> acc.append(elem) }
        }
    }

    /** Representation of a list node
     *  Holding data and pointer to the next
     *  node */
    inner class Node(val data: T, var next: Node? = null)

    /** Representation of a single linked list
     *  iterator. */
    inner class SingleLinkedListIterator: Iterator<T> {
        var cur = head

        override fun hasNext(): Boolean = cur != null

        override fun next(): T {
            val result = cur!!.data
            cur = cur?.next
            return result
        }
    }

    override fun iterator(): Iterator<T> = SingleLinkedListIterator()

    private var head: Node? = null
    private var tail: Node? = null
    private var size: Int = 0

    /**
     * Add new item to the beginning of the list.
     *
     * @param elem element to add
     */
    fun add(elem: T): SingleLinkedList<T> {
        head = Node(elem, head)
        if (head?.next == null) {
            tail = head
        }
        ++size
        return this
    }

    /**
     * Add new item to the given index.
     *
     * @param elem element to add
     * @throws IndexOutOfBoundsException
     */
    fun add(elem: T, index: Int): SingleLinkedList<T> {
        if (index !in 0..size) {
            throw IndexOutOfBoundsException()
        }
        when (index) {
            0 -> add(elem)
            size -> append(elem)
            else -> {
                var cur = head
                for (i in 0..(index - 2)) {
                    cur = cur!!.next
                }
                val newNode = Node(elem, cur!!.next)
                cur.next = newNode
                ++size
            }
        }
        return this
    }

    /**
     * Add element at the end of the list
     *
     * @param elem element to append to the list
     */
    fun append(elem: T): SingleLinkedList<T> {
        if (tail == null) {
            add(elem)
        } else {
            val newNode = Node(elem)
            tail?.next = newNode
            tail = newNode
            ++size
        }
        return this
    }

    /**
     * Get the element at the given index.
     *
     * @param index target index
     * @throws IndexOutOfBoundsException
     * @return element at the target index
     */
    operator fun get(index: Int): T {
        if (index !in 0..(size - 1)) {
            throw IndexOutOfBoundsException()
        }
        var cur = head
        for (i in 0..(index - 1)) {
            cur = cur?.next
        }
        return cur!!.data
    }

    /**
     * Reduce the list into a single value.
     *
     * @param zeroElement starting element
     * @param f reducing function
     */
    fun <U> fold(zeroElement: U, f: (acc: U, elem: T) -> U): U {
        var result = zeroElement
        var cur = head
        while (cur != null) {
            result = f(result, cur.data)
            cur = cur.next
        }
        return result
    }

    /**
     * Remove and return the element
     * at the given index.
     *
     * @param index target index
     * @throws IndexOutOfBoundsException if empty list
     * @return element at the target index
     */
    fun remove(index: Int): T? {
        if (index !in 0..(size - 1)) {
            throw IndexOutOfBoundsException()
        }
        if (index == 0) {
            val result = head!!.data
            head = head?.next
            --size
            if (head == null) {
                tail = null
            }
            return result
        }
        var cur = head
        for (i in 0..(index - 2)) {
            cur = cur?.next
        }
        val result = cur?.next?.data
        cur?.next = cur?.next?.next
        --size
        return result
    }

    /**
     * Reverse the underlying list
     *
     * This implementation is has a liner running
     * time and iterative
     */
    fun reverse() {
        if (size < 2) return
        var prev: Node?
        var cur = head
        var next = head?.next
        cur?.next = null
        while (next != null) {
            prev = cur
            cur = next
            next = next.next
            cur.next = prev
        }
        tail = head
        head = cur
    }

    /**
     * Clears the underlying list.
     */
    fun clear() {
        head = null
        tail = null
        size = 0
    }

    fun size(): Int = size
    fun isEmpty(): Boolean = size == 0
    fun nonEmpty(): Boolean = !isEmpty()

    override fun equals(other: Any?): Boolean {
        if (other !is SingleLinkedList<*>) return false
        val that = other as SingleLinkedList<*>?
        if (that?.size != size) return false
        var thatCur = that.head
        var cur = head
        while (cur != null && thatCur != null) {
            if (cur.data != thatCur.data) return false
            cur = cur.next
            thatCur = thatCur.next
        }
        return true
    }

    override fun hashCode(): Int {
        var result = 0
        var cur = head
        while (cur != null) {
            result = 31 * result + Objects.hash(cur.data)
            result %= (Integer.MAX_VALUE - 1)
            cur = cur.next
        }
        return result
    }

    override fun toString(): String {
        val sb = StringBuilder("[")
        var cur = head
        var i = 0
        while (cur != null) {
            if (i > 0) sb.append(", ")
            sb.append(cur.data)
            cur = cur.next
            ++i
        }
        sb.append("]")
        return sb.toString()
    }

    public override fun clone(): Any {
        return this.fold(SingleLinkedList<T>()) { acc, elem -> acc.append(elem) }
    }
}