package edu.algorithms

/**
 * Implementation of immutable list in kotlin
 *
 * @author tarek-nawara
 */
sealed class IList<T> {
    companion object {
        operator fun <T> invoke(vararg data: T): IList<T> {
            return data.foldRight(Empty()) { t, acc: IList<T> -> acc + t }
        }

        /**
         * @return empty list instance
         */
        fun <T> empty(): IList<T> = Empty()
    }

    /**
     * Add element to the beginning of the list.
     *
     * @param elem element to add
     * @return new list of the added element
     */
    abstract fun add(elem: T): IList<T>

    /**
     * Handy representation of the add function
     * as an operator.
     *
     * @param elem element to add
     * @return new list with the added element
     */
    operator fun plus(elem: T): IList<T> = add(elem)

    /**
     * Take the first n elements of the list, or
     * the entire list if n is bigger than the size
     * of the list.
     *
     * @param n number of elements to take
     * @return list of the elements
     */
    abstract fun take(n: Int): IList<T>

    /**
     * Remove the first n elements of the list
     * or the entire list if n is bigger than the
     * size of the list.
     *
     * @param n number of elements to remove
     * @return elements of the remaining elements
     */
    abstract fun drop(n: Int): IList<T>

    /**
     * Take elements from the beginning of the list
     * as long as a condition is met.
     *
     * @param predict condition to test
     * @return list of elements meeting the condition
     */
    abstract fun takeWhile(predict: (elem: T) -> Boolean): IList<T>

    /**
     * Remove elements from the beginning of the list
     * as long as a condition is met.
     *
     * @param predict condition to test with
     * @return the list after removing the elements
     */
    abstract fun dropWhile(predict: (elem: T) -> Boolean): IList<T>

    /**
     * Reduce the underlying list given a zero element,
     * reduction order will be left to right.
     *
     * @param zeroElement first element to start reduction with
     * @param f merging function
     * @return the result of applying the reduce function over the list.
     */
    abstract fun <U> foldLeft(zeroElement: U, f: (acc: U, elem: T) -> U): U

    /**
     * Transform the elements in the list and return
     * a new list containing the transformed elements.
     *
     * @param f transforming function
     * @return new list of the transformed elements
     */
    abstract fun <U> map(f: (elem: T) -> U): IList<U>

    /**
     * Append all the elements of one list at the end
     * of the underlying list.
     *
     * @param other list to append
     * @return new list consist of the concatenation of
     *         the two lists
     */
    abstract fun append(other: IList<T>): IList<T>

    /**
     * @return head of the list
     */
    abstract fun head(): T

    /**
     * @return the remaining of the list
     */
    abstract fun tail(): IList<T>

    /**
     * @return size of the list
     */
    abstract fun size(): Int

    /**
     * @return true if list is empty, false otherwise
     */
    abstract fun isEmpty(): Boolean

    /**
     * @return true if list is non empty, false otherwise
     */
    fun nonEmpty(): Boolean = !isEmpty()

    /**
     * Convert the underlying list to a Java list
     * @return Java list
     */
    fun toList(): List<T> {
        return foldLeft(ArrayList()) { acc: ArrayList<T>, e -> acc.add(e); acc }
    }
}

class Empty<T>(private val id: Int = 0) : IList<T>() {
    override fun add(elem: T): IList<T> = Cons(elem, this)

    override fun take(n: Int): IList<T> = this

    override fun drop(n: Int): IList<T> = this

    override fun takeWhile(predict: (elem: T) -> Boolean): IList<T> = this

    override fun dropWhile(predict: (elem: T) -> Boolean): IList<T> = this

    override fun <U> foldLeft(zeroElement: U, f: (acc: U, elem: T) -> U): U = zeroElement

    override fun <U> map(f: (elem: T) -> U): IList<U> = Empty()

    override fun append(other: IList<T>): IList<T> = other

    override fun head(): T {
        throw IllegalArgumentException("Calling head on empty list")
    }

    override fun tail(): IList<T> {
        throw IllegalArgumentException("Calling tail on empty list")
    }

    override fun size(): Int = 0

    override fun isEmpty(): Boolean = true

    override fun equals(other: Any?): Boolean {
        if (other === this) return true
        if (other !is Empty<*>) {
            return false
        }
        return true
    }

    override fun hashCode(): Int {
        return id
    }

    override fun toString(): String = "[]"
}

class Cons<T>(private val head: T, private val tail: IList<T>) : IList<T>() {
    override fun add(elem: T): IList<T> = Cons(elem, this)

    override fun take(n: Int): IList<T> {
        return if (n > 0) Cons(head, tail.take(n - 1))
        else Empty()
    }

    override fun drop(n: Int): IList<T> {
        return if (n == 0) this
        else tail.drop(n - 1)
    }

    override fun takeWhile(predict: (elem: T) -> Boolean): IList<T> {
        return if (predict(head)) Cons(head, tail.takeWhile(predict))
        else Empty()
    }

    override fun dropWhile(predict: (elem: T) -> Boolean): IList<T> {
        return if (predict(head)) tail.dropWhile(predict)
        else this
    }

    override fun <U> foldLeft(zeroElement: U, f: (acc: U, elem: T) -> U): U {
        return tail.foldLeft(f(zeroElement, head), f)
    }

    override fun <U> map(f: (elem: T) -> U): IList<U> = Cons(f(head), tail.map(f))

    override fun append(other: IList<T>): IList<T> = Cons(head, tail.append(other))

    override fun head(): T = head

    override fun tail(): IList<T> = tail

    override fun size(): Int = 1 + tail.size()

    override fun isEmpty(): Boolean = false

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Cons<*>) {
            return false
        }
        val that = other as Cons<*>?
        if (that!!.size() != this.size() || that.head != this.head) return false
        return that.tail == this.tail
    }

    override fun hashCode(): Int {
        var result = head?.hashCode() ?: 0
        result = 31 * result + tail.hashCode()
        return result
    }

    override fun toString(): String {
        val list = this.foldLeft(ArrayList()) { acc: ArrayList<T>, elem ->
            acc.add(elem)
            acc
        }
        return list.joinToString(", ", "[", "]")
    }
}