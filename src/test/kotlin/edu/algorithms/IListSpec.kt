package edu.algorithms

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.Assert.assertEquals

class IListSpec : Spek({
    describe("IList") {
        val list = IList(1, 2, 3, 4)

        it("the size method should work correctly") {
            assertEquals(4, list.size())
        }

        it("map function should work correctly") {
            val mapRes = IList(2, 4, 6, 8)
            assertEquals(mapRes, list.map { it * 2 })
        }

        it("fold left function should work correctly") {
            assertEquals(10, list.foldLeft(0) { x, y -> x + y})
        }

        it("take should work correctly") {
            val one = list.take(2)
            assertEquals(IList(1, 2), one)
            val two = list.take(1000)
            assertEquals(list, two)
        }

        it("drop should work correctly") {
            val one = list.drop(2)
            assertEquals(IList(3, 4), one)
            val two = list.drop(1000)
            assertEquals(IList.empty<Int>(), two)
        }

        it("takeWhile should work correctly") {
            val odd = list.takeWhile { x -> x % 2 == 1 }
            assertEquals(IList(1), odd)
        }

        it("dropWhile should work correctly") {
            val even = list.dropWhile { x -> x % 2 == 1 }
            assertEquals(IList(2, 3, 4), even)
        }
    }
})
