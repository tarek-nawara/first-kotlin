package edu.algorithms

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.describe
import org.jetbrains.spek.api.dsl.it
import org.junit.Assert.assertEquals

class SingleLinkedListSpec : Spek({
    describe("Single linked list sanity tests") {
        val list = SingleLinkedList<Int>()

        beforeEachTest {
            list.clear()
        }

        it("Adding element to the list should work") {
            list.add(1)
            assertEquals(1, list.size())
            assertEquals(1, list[0])
        }

        it("Adding element a target index should work") {
            list.add(1)
            list.add(2)
            list.add(3)
            list.add(4, 1)
            list.add(100, 3)
            list.add(200, 5)
            println(list)
            assertEquals(4, list[1])
            assertEquals(6, list.size())
        }

        it("clear, isEmpty and nonEmpty should all work") {
            assertEquals(true, list.isEmpty())
            assertEquals(false, list.nonEmpty())
        }

        it("reverse should work") {
            list.add(1)
            list.add(2)
            list.add(3)
            val other = list.clone() as SingleLinkedList<*>
            list.reverse()
            println(list)
            list.reverse()
            println(list)
            assertEquals(other, list)
        }

        it("reverse with empty list should work") {
            val other = list.clone() as SingleLinkedList<*>
            list.reverse()
            assertEquals(other, list)
        }

        it("reverse with single element should work") {
            list.add(1)
            val other = list.clone() as SingleLinkedList<*>
            list.reverse()
            assertEquals(other, list)
        }

        it("remove should work correctly") {
            list.add(1)
            list.add(2)
            list.add(3)
            list.remove(1)
            assertEquals(2, list.size())
            println(list)
            assertEquals(3, list[0])
            assertEquals(1, list[1])
        }

        it("test invoke method") {
            val l = SingleLinkedList(1, 2, 3)
            assertEquals(3, l.size())
            for (i in 0..(l.size() - 1)) {
                assertEquals(i + 1, l[i])
            }
        }

        it("append should work") {
            val l = SingleLinkedList<Int>()
            l.append(1).append(2).append(3)
            println(l)
            assertEquals(3, l.size())
        }

        it("fold should work") {
            val l = SingleLinkedList(1, 2, 3)
            val result = l.fold(0) { acc, e -> acc + e }
            assertEquals(6, result)
        }
    }
})